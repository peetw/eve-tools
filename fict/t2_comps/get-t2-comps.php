<?php
// Only allow viewing from other scripts
if ($_POST) {
	// Get config data
	require_once($_SERVER['DOCUMENT_ROOT'] . '/resources/config.php');

	// Get variables
	$year;
	if (isset($_POST['year'])) {
		$year = $_POST['year'];
	}
	$month;
	if (isset($_POST['month'])) {
		$month = $_POST['month'];
	}
	$day;
	if (isset($_POST['day'])) {
		$day = $_POST['day'];
	}
	
	function getPrices($conn, $date) {
		// Get prices from DB
		$query = "SELECT item, jita_price, manufacture_cost FROM t2_comps WHERE date='".$date."'";
		$result = $conn->query($query);
		$num = $result->num_rows;
	
		// Validate data
		if ($num == 0) {
			die('<p><b>Data missing...</b></p>');
		} else if ($num != 47) {
			die('<p><b>Data corrupted...</b></p>');
		}

		// Export material prices
		echo "<table border='1'>
		<tr>
		<th>Moon Materials</th>
		<th>Jita Price</th>
		</tr>";

		for ($i = 0; $i < 11; $i++) {
			$row = $result->fetch_assoc();
				echo '<tr>';
				echo '<td>'.preg_replace('/_/', ' ', $row['item']).'</td>';
				echo '<td>'.number_format($row['jita_price'], 2).'</td>';
				echo '</tr>';
		}
		echo '</table>';
		echo '<br />';
		echo '<p>All prices rounded to nearest whole number</p>';
		echo '<br />';

		// Export component prices
		for ($i = 11; $i < 47; $i++) {
			$row = $result->fetch_assoc();

			if (($i-11) % 9 == 0 ) {
				echo "<table border='1'>";
				echo '<tr>';

				if ($i == 11)
					echo '<th>Amarr Components</th>';
				else if ($i == 20)
					echo '<th>Caldari Components</th>';
				else if ($i == 29)
					echo '<th>Gallente Components</th>';
				else if ($i == 38)
					echo '<th>Minmatar Components</th>';

				echo '<th>Jita Price</th>';
				echo '<th>Cost Price</th>';
				echo '</tr>';
			}

			echo '<tr>';
			echo '<td>' . preg_replace('/_/',' ',$row['item']) . '</td>';
			echo '<td>' . number_format($row['jita_price']) . '</td>';
			echo '<td>' . number_format($row['manufacture_cost']) . '</td>';
			echo '</tr>';

			if (($i-19) % 9 == 0 ) {
				echo '</table>';
				echo '<br />';
			}
		}
		// Free results
		$result->close();
	}


	function getDates($conn, $year, $month) {
		// Get dates from DB
		if (empty($year) && empty($month)) {
			$query = 'SELECT DISTINCT date FROM t2_comps';
			echo '<option>Year:</option>';
		} else if (empty($month)) {
			$query = "SELECT DISTINCT date FROM t2_comps WHERE date LIKE '".$year."-%'";
			echo '<option>Month:</option>';
		} else {
			$query = "SELECT DISTINCT date FROM t2_comps WHERE date LIKE '".$year."-".$month."-%'";
			echo '<option>Day:</option>';
		}
		$result = $conn->query($query);

		// List all dates in range
		$date_old = 0;
		while ($row = $result->fetch_row()) {
			if (empty($year) && empty($month)) {
				$date = substr($row[0], 0, 4);
			} else if (empty($month)) {
				$date = substr($row[0], 5, 2);
			} else {
				$date = substr($row[0], 8, 2);
			}

			if ($date != $date_old) {
				echo '<option>'.$date.'</option>';
				$date_old = $date;
			}
		}
		// Free results
		$result->close();
	}
	
	// Connect to DB
	$conn = new mysqli($DB['eve_tools']['host'], $DB['eve_tools']['user'], $DB['eve_tools']['pass'], $DB['eve_tools']['name']);
	if ($conn->connect_errno) {
		die('<option>' . $conn->connect_error . '</option>');
	}

	// Check whether to return price list or dates
	if (isset($year) && isset($month) && isset($day)) {
		$date = $year . '-' . $month . '-' . $day;
		getPrices($conn, $date);
	} else {
		getDates($conn, $year, $month);
	}

	// Close DB connection
	$conn->close();
} else {
	// Redirect back to main page
	header('Location: index.php');
}
?>
