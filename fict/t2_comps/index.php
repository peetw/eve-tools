<?php
	// Page title
	$page_title = "T2 Ship Components - EVE Tools";

	// Load up config file and header
	require_once($_SERVER["DOCUMENT_ROOT"] . "/resources/config.php");
	require_once(TEMPLATES_PATH . "/header.php");
?>

<div class="container">
	<p>Select a date to display price data from:</p>

	<!-- Drop down menus for date selection -->
	<form>
		<select name='yearList' id='yearList'>
			<option value=''>Year:</option>
		</select>
		<select name='monthList' id='monthList'>
			<option value=''>Month:</option>
		</select>
		<select name='dayList' id='dayList'>
			<option value=''>Day:</option>
		</select>
	</form>

	<!-- Container to display prices once date selected -->
	<br />
	<div id='priceList'></div>
</div>

<!-- Load jQuery -->
<script type='text/javascript' src='//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js'></script>
<!-- jQuery scripts -->
<script type='text/javascript' src='/js/t2-comps.js'></script>

<?php
	require_once(TEMPLATES_PATH . "/footer.php");
?>
