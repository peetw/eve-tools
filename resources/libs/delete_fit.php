<?php
	// Load up config file
#	require_once($_SERVER["DOCUMENT_ROOT"] . "/resources/config.php");

	// Function to remove fit from DB
	function delete_fit($id, $DB) {
		// Create array to hold errors
		$errors = array();
		
		// Connect to eve_tools DB
		$conn = new mysqli($DB['eve_tools']['host'], $DB['eve_tools']['user'], $DB['eve_tools']['pass'], $DB['eve_tools']['name']);
		if ($conn->connect_errno) {
			$errors[] = $conn->connect_error;
		} else {
			$query = "DELETE FROM `kazo_ships` WHERE `id`=?";
			if ($stmt = $conn->prepare($query)) {
				$stmt->bind_param('i', $id);
				$stmt->execute();
				$stmt->close();
			} else {
				$errors[] = $conn->error;
			}
		}
		
		// Return any errors
		return $errors;
	}
?>
