<?php
	// Load up config file
	require_once($_SERVER["DOCUMENT_ROOT"] . "/resources/config.php");

	// Create array to hold errors
	$errors = array();

	// Connect to DB
	$conn = new mysqli($DB['eve_tools']['host'], $DB['eve_tools']['user'], $DB['eve_tools']['pass'], $DB['eve_tools']['name']);
	if ($conn->connect_errno) {
		$errors[] = $conn->connect_error;
	} else {
		// Return a list of ships and their fits and stock levels from the DB
		$query = "SELECT * FROM `kazo_ships` ORDER BY `hull`, `fit`";
		$res = $conn->query($query);
		
		// Check whether there are currently any stored fits
		if ($res->num_rows) {
			// Loop through fits and generate links etc
			while ($row = $res->fetch_assoc()) {
				echo '<tr>';
				echo '<td>'.$row['hull'].'</td>';
				echo '<td><a href="view_fit.php?id='.$row['id'].'">'.$row['fit'].'</a></td>';
				echo '<td><form method="post"><input type="text" name="currentNum" value='.$row['current'].' placeholder='.$row['current'].' size=2></td>';
				echo '<td><form method="post"><input type="text" name="desiredNum" value='.$row['desired'].' placeholder='.$row['desired'].' size=2></td>';
				echo '<td><button id="update" name="updateID" type="submit" value='.$row['id'].'>Go</button></form></td>';
				echo '<td><form method="post"><button id="delete" name="deleteID" type="submit" value='.$row['id'].'>X</button></form></td>';
				echo '</tr>';
			}
		} else {
			$errors[] = 'DB contains no entries, would you like to add one?';
		}
	}
?>
