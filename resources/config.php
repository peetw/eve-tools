<?php

/*
	The important thing to realize is that the config file should be included in every
	page of your project, or at least any page you want access to these settings.
	This allows you to confidently use these settings throughout a project because
	if something changes such as your database credentials, or a path to a specific resource,
	you'll only need to update it here.
*/

$TOP_NAV = array(
	0 => array (
		"url" => "/",
		"name" => "home"
	),
	1 => array (
		"url" => "/fict/",
		"name" => "fict"
	),
	2 => array (
		"url" => "/kazo/",
		"name" => "kazo"
	),
	3 => array (
		"url" => "/market/",
		"name" => "market"
	),
	4 => array (
		"url" => "/user/",
		"name" => "user admin"
	),
);

$DB = array(
	"eve_tools" => array(
		"host" => "localhost",
		"user" => "eve_tools",
		"pass" => "eve_tools_pw",
		"name" => "eve_tools"
	),
	"eve_tools_t2_comps" => array(
		"host" => "localhost",
		"user" => "eve_tools",
		"pass" => "eve_tools_pw",
		"name" => "eve_tools_t2_comps"	
	),
	"eve_static" => array(
		"host" => "localhost",
		"user" => "eve_tools",
		"pass" => "eve_tools_pw",
		"name" => "eve_static_dump"
	),
	"yapeal" => array(
		"host" => "localhost",
		"user" => "eve_tools",
		"pass" => "eve_tools_pw",
		"name" => "yapeal"
	)
);

$CONF = array(
	"urls" => array(
		"baseUrl" => "http://example.com"
	),
	"paths" => array(
		"resources" => $_SERVER["DOCUMENT_ROOT"] . "/resources",
		"images" => array(
			"content" => $_SERVER["DOCUMENT_ROOT"] . "/imgs/content",
			"layout" => $_SERVER["DOCUMENT_ROOT"] . "/imgs/layout"
		)
	)
);

/*
	I will usually place the following in a bootstrap file or some type of environment
	setup file (code that is run at the start of every page request), but they work 
	just as well in your config file if it's in php (some alternatives to php are xml or ini files).
*/

/*
	Creating constants for heavily used paths makes things a lot easier.
	ex. require_once(LIBRARY_PATH . "Paginator.php")
*/
defined("LIB_PATH")
	or define("LIB_PATH", realpath(dirname(__FILE__) . '/libs'));
	
defined("TEMPLATES_PATH")
	or define("TEMPLATES_PATH", realpath(dirname(__FILE__) . '/templates'));

/*
	Error reporting.
*/
ini_set("error_reporting", "true");
error_reporting(E_ALL|E_STRCT);

?>
