CREATE TABLE `t2_comps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item` varchar(64) NOT NULL,
  `jita_price` decimal(8,2) NOT NULL,
  `manufacture_cost` decimal(8,2) DEFAULT NULL,
  `date` DATE NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO t2_comps (item,jita_price,manufacture_cost,date) SELECT item,jita_price,cost_price,'2012-04-06' FROM eve_tools_t2_comps.2012_04_06;
INSERT INTO t2_comps (item,jita_price,manufacture_cost,date) SELECT item,jita_price,cost_price,'2012-04-09' FROM eve_tools_t2_comps.2012_04_09;
INSERT INTO t2_comps (item,jita_price,manufacture_cost,date) SELECT item,jita_price,cost_price,'2012-04-11' FROM eve_tools_t2_comps.2012_04_11;
INSERT INTO t2_comps (item,jita_price,manufacture_cost,date) SELECT item,jita_price,cost_price,'2012-04-18' FROM eve_tools_t2_comps.2012_04_18;
INSERT INTO t2_comps (item,jita_price,manufacture_cost,date) SELECT item,jita_price,cost_price,'2012-04-23' FROM eve_tools_t2_comps.2012_04_23;
INSERT INTO t2_comps (item,jita_price,manufacture_cost,date) SELECT item,jita_price,cost_price,'2012-04-26' FROM eve_tools_t2_comps.2012_04_26;
INSERT INTO t2_comps (item,jita_price,manufacture_cost,date) SELECT item,jita_price,cost_price,'2012-07-06' FROM eve_tools_t2_comps.2012_07_06;
INSERT INTO t2_comps (item,jita_price,manufacture_cost,date) SELECT item,jita_price,cost_price,'2012-07-07' FROM eve_tools_t2_comps.2012_07_07;
INSERT INTO t2_comps (item,jita_price,manufacture_cost,date) SELECT item,jita_price,cost_price,'2012-08-30' FROM eve_tools_t2_comps.2012_08_30;
INSERT INTO t2_comps (item,jita_price,manufacture_cost,date) SELECT item,jita_price,cost_price,'2012-09-16' FROM eve_tools_t2_comps.2012_09_16;
INSERT INTO t2_comps (item,jita_price,manufacture_cost,date) SELECT item,jita_price,cost_price,'2012-09-21' FROM eve_tools_t2_comps.2012_09_21;
INSERT INTO t2_comps (item,jita_price,manufacture_cost,date) SELECT item,jita_price,cost_price,'2012-10-10' FROM eve_tools_t2_comps.2012_10_10;
INSERT INTO t2_comps (item,jita_price,manufacture_cost,date) SELECT item,jita_price,cost_price,'2012-10-18' FROM eve_tools_t2_comps.2012_10_18;
INSERT INTO t2_comps (item,jita_price,manufacture_cost,date) SELECT item,jita_price,cost_price,'2012-10-30' FROM eve_tools_t2_comps.2012_10_30;
INSERT INTO t2_comps (item,jita_price,manufacture_cost,date) SELECT item,jita_price,cost_price,'2012-11-07' FROM eve_tools_t2_comps.2012_11_07;
INSERT INTO t2_comps (item,jita_price,manufacture_cost,date) SELECT item,jita_price,cost_price,'2012-11-13' FROM eve_tools_t2_comps.2012_11_13;
INSERT INTO t2_comps (item,jita_price,manufacture_cost,date) SELECT item,jita_price,cost_price,'2012-11-20' FROM eve_tools_t2_comps.2012_11_20;
INSERT INTO t2_comps (item,jita_price,manufacture_cost,date) SELECT item,jita_price,cost_price,'2012-11-29' FROM eve_tools_t2_comps.2012_11_29;
INSERT INTO t2_comps (item,jita_price,manufacture_cost,date) SELECT item,jita_price,cost_price,'2013_02-07' FROM eve_tools_t2_comps.2013_02_07;
INSERT INTO t2_comps (item,jita_price,manufacture_cost,date) SELECT item,jita_price,cost_price,'2013_02-20' FROM eve_tools_t2_comps.2013_02_20;
INSERT INTO t2_comps (item,jita_price,manufacture_cost,date) SELECT item,jita_price,cost_price,'2013_03-01' FROM eve_tools_t2_comps.2013_03_01;
INSERT INTO t2_comps (item,jita_price,manufacture_cost,date) SELECT item,jita_price,cost_price,'2013_03-09' FROM eve_tools_t2_comps.2013_03_09;