CREATE TABLE `kazo_fit_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kazo_ship_id` int(10) unsigned NOT NULL,
  `item` varchar(128) NOT NULL,
  `slot` varchar(16) NOT NULL,
  `quantity` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`kazo_ship_id`) REFERENCES `kazo_ships` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

