CREATE TABLE `kazo_ships` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hull` varchar(64) NOT NULL,
  `fit` varchar(128) NOT NULL,
  `current` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `desired` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
