<!DOCTYPE HTML>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
	<title><?php echo $page_title; ?></title>
	
	<!--[if lt IE 9]>
		<script src="/js/html5shiv.js"></script>
	<![endif]-->
	
	<link rel="stylesheet" type="text/css" href="/css/main.css" />
</head>

<body>
<header>
	<nav class="topNav">
		<ul>
			<?php
			foreach ($TOP_NAV as $i => $nav) {
				$active = "";
				$baseurl = preg_replace('/^(\/[a-zA-Z0-9]*\/*).*/', '$1', $_SERVER['REQUEST_URI']);
				if ($baseurl == $nav['url']) {
					$active = "active";
				}
				echo '<li id="'.$active.'"><a href="'.$nav['url'].'">'.$nav['name'].'</a></li>';
			}
			?>
		</ul>
	</nav>
</header>
