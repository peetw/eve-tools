<?php
	// Page title
	$page_title = "User Admin - EVE Tools";

	// Load up config file and header
	require_once($_SERVER["DOCUMENT_ROOT"] . "/resources/config.php");
	require_once(TEMPLATES_PATH . "/header.php");
?>

<div class="container">
		Redirect to login.php if not logged in...
</div>

<?php
	require_once(TEMPLATES_PATH . "/footer.php");
?>
