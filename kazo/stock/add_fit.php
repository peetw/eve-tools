<?php
	// Page title
	$page_title = "Add Fit - EVE Tools";

	// Load up config file and header
	require_once($_SERVER["DOCUMENT_ROOT"] . "/resources/config.php");
	require_once(TEMPLATES_PATH . "/header.php");
?>

<?php
// Parse user entered text and store fit in DB
if ($_POST) {
	// Create array to hold errors
	$errors = array();
	
	// Validate input text as non-empty and store text in array
	$text;
	if (empty($_POST['textEFT'])) {
		$errors[] =  'Please enter a valid EFT fitting1';
	} else {
		$text = explode("\n", trim($_POST['textEFT']));
		$text = array_map('trim', $text);
	}

	// Validate fit is in EFT format
	$header = $text[0];
	if (!preg_match('/^\[.+, .+\]/', $header) || count($text) < 10) {
		$errors[] = 'Please enter a valid EFT fitting';
	}

	// Connect to eve_tools DB
	$conn = new mysqli($DB['eve_tools']['host'], $DB['eve_tools']['user'], $DB['eve_tools']['pass'], $DB['eve_tools']['name']);
	if ($conn->connect_errno) {
		$errors[] = $conn->connect_error;
	} else {
		// Check fit doesn't already exist in DB
		$hull = preg_replace('/^\[(.*), *.*\]/', '$1', $header);
		$fit = preg_replace('/^\[.*, *(.*)\]/', '$1', $header);
		$query = "SELECT `id` FROM `kazo_ships` WHERE `hull` = ? AND `fit` = ?";
		if ($stmt = $conn->prepare($query)) {
			$stmt->bind_param('ss', $hull, $fit);
			$stmt->execute();
			$stmt->store_result();
			if ($stmt->num_rows)
				$errors[] = 'Fit already exists';
			$stmt->free_result();
			$stmt->close();
		} else {
			$errors[] = 'Prepared statement broken';
		}
	}

	// Process EFT fit
	if (!$errors) {
		$processed = array();
		$slot = array('LOW', 'MID', 'HIGH', 'RIG', 'DRONE');
		$j = 0;
		for($i = 1; $i < count($text); $i++) {
			// Get item name
			$item = $text[$i];

			// Check current slot position and process
			if ($item) {
				// Prefix item with slot position
				$item = $slot[$j] . ' ' . $item;

				// Remove ammo & scripts by removing anything after a comma
				$item = preg_replace('/,.*/', '', $item);

				// Check for drones and expand to correct number
				if (preg_match('/^.* x[0-9]+.*/', $item)) {
					$numDrones = preg_replace('/^.* x([0-9]+).*/', '$1', $item);
					$item = preg_replace('/^(.*) x[0-9]+.*/', '$1', $item);
					for ($k = 0; $k < $numDrones; $k++) {
						$processed[] = $item;
					}
				} else {
					$processed[] = $item;
				}
			} else if (!$item && trim($text[$i+1]) && $i > 1) {
				$j++;
			}
		}

		// Count occurance of each module
		$processed = array_count_values($processed);

		// Store fit meta details in DB
		$id;
		$query = "INSERT INTO `kazo_ships` (`hull`, `fit`) VALUES (?, ?)";
		if ($stmt = $conn->prepare($query)) {
			$stmt->bind_param('ss', $hull, $fit);
			$stmt->execute();
			$id = $stmt->insert_id;
			$stmt->close();
		} else {
			$errors[] = 'Could not store meta details in DB';
		}

		// Store actual fit in DB
		$query = "INSERT INTO `kazo_fit_items` (`kazo_ship_id`, `item`, `slot`, `quantity`) VALUES (?, ?, ?, ?)";
		if ($stmt = $conn->prepare($query)) {
			// Bind params
			$stmt->bind_param('issi', $id, $item, $slot, $quantity);
	
			// Loop through each item and add to DB
			foreach ($processed as $item => $quantity) {
				$slot = preg_replace('/^([A-Z]+) .*/', '$1', $item);
				$item = preg_replace('/^[A-Z]+ (.*)/', '$1', $item);
				$stmt->execute();
			}
	
			// Close statement
			$stmt->close();
		} else {
			$errors[] = 'Could not store fit in DB';
		}

		// Close DB connection
		$conn->close();

		// Redirect to main page on success to clear $_POST
#		header('Location: '.$_SERVER['REQUEST_URI']);
		header('Location: index.php');
		exit();
	}
}
?>

<div class="container">
	<h1>Add A Ship Fitting</h1>
	
	<form method="post">
		<?php require_once(TEMPLATES_PATH . "/errors.php"); ?>
		Copy and paste from EFT (currently not working for T3 cruisers):
		<div class="clear"></div>
		<textarea name="textEFT" style="width: 500px; height: 500px; resize: none;" required></textarea>
		<div class="clear"></div>
		<input type="submit" value="Add fit">
	</form>	
</div>

<?php
	require_once(TEMPLATES_PATH . "/footer.php");
?>
