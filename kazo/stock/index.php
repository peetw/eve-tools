<?php
	// Page title
	$page_title = "KAZO Stock - EVE Tools";

	// Load up config file and header
	require_once($_SERVER["DOCUMENT_ROOT"] . "/resources/config.php");
	require_once(TEMPLATES_PATH . "/header.php");
	require_once(LIB_PATH . "/delete_fit.php");
?>

<?php
	// Create array to hold errors
	$errors = array();

	// Check for $_POST variables
	if ($_POST['deleteID']) {
		// Delete specified fit and retrieve any erros
		$errors = delete_fit($_POST['deleteID'], $DB);
		
		// Redirect to this page to clear $_POST, otherwise continue to display errors
		if (!$errors) {
			header('Location: '.$_SERVER['REQUEST_URI']);
			exit();
		}
	}
?>

<div class="container">
	<h1>Stock Levels</h1>
	<form action="add_fit.php" method="get">
		<input type="submit" value="Add fit">
	</form> 
	<p>Click on the fit name to view EFT fitting</p>
	<table id="stock", border="1">
		<tr>
			<td>Hull</td>
			<td>Fitting</td>
			<td>Current Stock</td>
			<td>Desired Stock</td>
			<td>Update</td>
			<td>Delete</td>
			<?php include(LIB_PATH . '/get_stock.php'); ?>
		</tr>
	</table>
</div>

<?php
	require_once(TEMPLATES_PATH . "/footer.php");
?>
