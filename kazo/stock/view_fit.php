<?php
	// Page title
	$page_title = "View Fit - EVE Tools";

	// Load up config file and header
	require_once($_SERVER["DOCUMENT_ROOT"] . "/resources/config.php");
	require_once(TEMPLATES_PATH . "/header.php");
?>

<div class="container">
	<h1>View A Ship Fitting</h1>
	<textarea style="width: 500px; height: 500px; resize: none;" readonly>
<?php
	// Create array to hold errors
	$errors = array();

	// Check that correct variables passed via URL
	if (empty($_GET['id'])) {
		$errors[] = 'Incorrect args';
	} else {
		// Assign variables
		$id = $_GET['id'];

		// Connect to DB
		$conn = new mysqli($DB['eve_tools']['host'], $DB['eve_tools']['user'], $DB['eve_tools']['pass'], $DB['eve_tools']['name']);
		if ($conn->connect_errno) {
			$errors[] = $conn->connect_error;
		} else {
			// Get fit details
			$query = "SELECT `hull`, `fit`, `item`, `slot`, `quantity` FROM `kazo_ships`,`kazo_fit_items` WHERE `kazo_ships`.`id`=? AND `kazo_fit_items`.`kazo_ship_id`=?";
			if ($stmt = $conn->prepare($query)) {
				$stmt->bind_param('ii', $id, $id);
				$stmt->execute();
				$res = $stmt->get_result();
				$stmt->close();
				
				// Check that the given id corresponds to an actual fit
				if ($res->num_rows) {

					// Loop through fit modules and drones
					$slot;
					while ($row = $res->fetch_assoc()) {
						// Get current slot position
						if (empty($slot)) {
							$slot = $row['slot'];
							echo "[".$row['hull'].", ".$row['fit']."]\n";
						} else if ($slot != $row['slot']) {
							$slot = $row['slot'];
							echo "\n";
						}
						
						// Output item(s)
						if ($slot == "DRONE") {
							echo $row['item']." x".$row['quantity']."\n";
						} else {
							for($i = 0; $i < $row['quantity']; $i++) {
								echo $row['item']."\n";
							}
						}
					}
				} else {
					$errors[] = 'ID does not correspond to an actual fit';
				}
			} else {
				$errors[] = 'Could not prepare statement';
			}
		}
	}
?>
</textarea>
</div>

<?php
	require_once(TEMPLATES_PATH . "/footer.php");
?>
