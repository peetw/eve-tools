<?php
	// Page title
	$page_title = "POS List - EVE Tools";

	// Load up config file and header
	require_once($_SERVER["DOCUMENT_ROOT"] . "/resources/config.php");
	require_once(TEMPLATES_PATH . "/header.php");
?>

<div class="container">
	<h1>Current POS</h1>
	
	<table id="posList", border="1">
		<tr>
			<td>Location</td>
			<td>POS</td>
			<td>Current Status</td>
			<td>Fuel Blocks</td>
			<td>Charters</td>
			<td>Fuel Remaining</td>
			<td>Stront</td>
			<td>Reinforcement Timer</td>
		</tr>
		<?php
		$_POST['corp'] = 'kazo';
		include_once(LIB_PATH . "/get-pos.php");
		?>
	</table>
</div>

<?php
	require_once(TEMPLATES_PATH . "/footer.php");
?>
