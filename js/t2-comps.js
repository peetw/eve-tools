$(document).ready(function() {
	// External PHP file
	var url = 'get-t2-comps.php';

	// Initialise year list once document loaded
	$('#yearList').load(url, { year: '' });
	
	// Update month list when year selected
	$('#yearList').change(function() {
		$('#priceList').html('');
		$('#dayList').html('<option>Day:</option>');
		var year = $('#yearList').val();
		$('#monthList').load(url, { year: year });
	});
	// Update day list when month selected
	$('#monthList').change(function() {
		$('#priceList').html('');
		var year = $('#yearList').val();
		var month = $('#monthList').val();
		$('#dayList').load(url, { year: year, month: month });
	});
	// Generate price list when day selected
	$('#dayList').change(function() {
		var year = $('#yearList').val();
		var month = $('#monthList').val();
		var day = $('#dayList').val();
		$('#priceList').load(url, { year: year, month: month, day: day });
	});
});
