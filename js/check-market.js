var i;
var timeout;

function start_lookup_moon_mats() {
	i = 0;
	timeout = setInterval("lookup_moon_mats()",2000);
}

function start_lookup_comps() {
	timeout = setInterval("lookup_comps()",2000);
}


function lookup_moon_mats() {
	var mat_ids = Array(16670,17317,16673,16683,16679,16682,16681,16680,16678,16671,16672);

	if (i < 11) {
		CCPEVE.showMarketDetails(mat_ids[i]);
		i++;
    } else {
		clearInterval(timeout);
		i = 0;
		start_lookup_comps();
	}
}

function lookup_comps() {
	var comp_ids = Array(11549,11694,11532,11689,11557,11539,11537,11554,11543,11534,11693,11550,11533,11540,11552,11690,11558,11544,11545,11547,11531,11535,11553,11688,11541,11695,11556,11555,11551,11542,11536,11538,11692,11548,11530,11691);

	if (i < 36) {
		CCPEVE.showMarketDetails(comp_ids[i]);
		i++;
	} else {
		clearInterval(timeout);
    }
}
